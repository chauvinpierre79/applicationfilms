## Utilisateurs
    UserOnline
    UserOffline



## Contextes

**Authentification**
    - En tant que visiteur hors-ligne je veux pouvoir créer un compte
    - En tant que visiteur hors-ligne je veux pouvoir me connecter
    - En tant que visiteur en ligne je veux pouvoir gérer mon profil

**Forum**
    - En tant que visiteur en ligne je veux pouvoir laisser une appréciation sur un film
    - En tant que visiteur en ligne je veux échanger avec des personnes qui ont regarder un film en particulier
    - 

**MovieBox**
    - En tant que visiteur hors-ligne je veux découvrir des films
    - En tant que visiteur hors-ligne je veux me renseigner sur la filmographie d'un/une acteur/actrice
    - En tant que visiteur hors-ligne je veux avoir un déscriptif des films