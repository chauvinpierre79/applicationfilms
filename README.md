# Sujet

Ce projet a pour but de vous faire découvrir des films ou séries tv selon vos préférences.

On est dimanche soir et vous ne savez pas quoi regarder pour terminer votre Week end ? Vous passez un temps fou à cherche un film ou une série à regarder ? Cette application est faite pour vous !!

Le fonctionnement est simple, l’application web vous propose une liste de critères à définir
-	Type de l’œuvre (film ou serie)
-	Genre de l’œuvre (science-fiction, fantaisie, romance, film d’horreur, etc..)
-	Année de parution

L’application vous retourne ensuite une liste de films et/ou series tv correspondant à votre recherche.
